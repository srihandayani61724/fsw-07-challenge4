const http = require('http');
// inisialisasi port 3000
const {
    port = 8000
} = process.env;

// import fungsi fs
const fs = require('fs');
// import path
const path = require('path');
// import PUBLIC_DIRECTORY
const PUBLIC_DIRECTORY = path.join(__dirname, '../public');

// buat fungsi untuk render file html
function renderHTML(htmlFileName) {
    return fs.readFileSync(path.join(PUBLIC_DIRECTORY, htmlFileName), 'utf8');
}

// buat fungsi request handler
function onRequest(req, res) {
    switch (req.url) {
        case '/':
            res.writeHead(200, {
                'Content-Type': 'text/html'
            });
            return res.end(renderHTML('index.html'));
        case '/cars':
            res.writeHead(200, {
                'Content-Type': 'text/html'
            });
            return res.end(renderHTML('cari-mobil.html'));
        default:
            if ((req.url.indexOf('.css') != -1) || (req.url.indexOf('.png') != -1) || (req.url.indexOf('.js') != -1) || (req.url.indexOf('.jpg') != -1)) {
                const cssFilePath = path.join(PUBLIC_DIRECTORY, req.url);
                fs.readFile(cssFilePath, function (err, data) {
                    res.writeHead(200, {
                        'Content-Type': ['text/css', 'image/png', 'image/jpg']
                    })
                    res.write(data);
                    return res.end();
                    // console.log(data)
                    // console.log(cssFilePath)
                })
            } else {
               res.writeHead(404, {
                   'Content-Type': 'text/html'
                });
               res.end('<h1>404 Not Found</h1>');
            }
    }
}

// buat server
const server = http.createServer(onRequest);

// jalankan server
server.listen(port, 'localhost', () => {
    console.log(`Server running at http://localhost:%d`, port);
});