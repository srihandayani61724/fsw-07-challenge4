class App {
  constructor() {
    // this.clearButton = document.getElementById("clear-btn");
    // this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.buttonSubmitData = document.getElementById("submitFilter")
    this.inputTipe = document.getElementById("inputTipe")
    this.inputDate = document.getElementById("inputDate")
    this.inputTime = document.getElementById("inputTime")
    this.inputPenumpang = document.getElementById("inputPenumpang")
  }

  async init() {
    await this.load();

    // Register click listener
    // this.clearButton.onclick = this.clear;
    // this.loadButton.onclick = this.run;
    this.buttonSubmitData.onclick = this.run;
    this.run;
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.classList.add('col-lg-4')
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);

    this.buttonSubmitData.addEventListener('click', async (e) =>{
      let inputTipeVal = this.inputTipe.options[this.inputTipe.selectedIndex].value
      let inputDateVal = this.inputDate.value
      let inputTimeVal = this.inputTime.options[this.inputTime.selectedIndex].value
      let inputPenumpangVal = this.inputPenumpang.value
      this.clear()

      let cars = await Binar.listCars((car) => {
        let result = true;

        let dateTime = inputDateVal + "T" + inputTimeVal;
        if ((!isNaN(Date.parse(dateTime))) && (!isNaN(parseInt(inputPenumpangVal)))){
          result = (car.availableAt.getTime() >= Date.parse(dateTime)) && (car.capacity >= parseInt(inputPenumpangVal));
        }

        if(!isNaN(Date.parse(dateTime))){
          result = car.availableAt.getTime() >= Date.parse(dateTime)
        }

        if(!isNaN(parseInt(inputPenumpangVal))){
          result = car.capacity === parseInt(inputPenumpangVal)
        }

        return result;
      })

      console.log(cars);

      Car.init(cars)
    })
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}