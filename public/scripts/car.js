class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
  <div class="col mt-3">
    <div class="card">
      <img src="${this.image}" class="card-img-top daftar-gambar-mobil" alt="...">
      <div class="card-body">
        <p class="card-text">${this.type}</p>
        <strong>
          <p>Rp ${this.rentPerDay} / Hari</p>
        </strong>
        <p class="deskripsi-card">${this.description}</p>
        <ul class="m-0 p-0">
          <li><img src="./images/img/fi_users.png" alt="">${this.capacity}  orang</li>
          <li><img src="./images/img/fi_settings.png" alt="">${this.transmission}</li>
          <li><img src="./images/img/fi_calendar.png" alt="">Tahun ${this.year}</li>
        </ul>
      <div class="d-grid gap-2 mt-3">
        <button class="btn btn-success" type="button">Pilih Mobil</button>
      </div>
    </div>
  </div>
</div>

    `;
  }
}